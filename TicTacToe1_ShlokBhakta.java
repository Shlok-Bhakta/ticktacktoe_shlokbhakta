/*
Shlok Bhakta
10/4/2021
Tick Tac Toe 
*/
package tictactoe2_shlokbhakta;


import java.util.*;
import javax.swing.*;


public class TicTacToe2_ShlokBhakta {

		static int turn = 0;
	public static void main(String[] args) {
		System.out.println("The Game has a minor bug in that the instructions ask for O to be the Computer but 10+ hours in I have realized this mistake umm... \nCtrl+H didnt fix it and it is 10:00PM on monday so I gotta get some more stuff done.");
		//asking for 1 or 2 player mode
		boolean win = false;

		String[] gameOptions = {"Solo(with bot)", "with a friend"};
		int mode = JOptionPane.showOptionDialog(null, "Would you like to play alone or with a friend?", "Gamemode", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, gameOptions, gameOptions[0]);
		String[] board = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
		ArrayList<String> available = new ArrayList<String>();
		available.add("1");
		available.add("2");
		available.add("3");
		available.add("4");
		available.add("5");
		available.add("6");
		available.add("7");
		available.add("8");
		available.add("9");

		int change = 0;
		String character;
		//singleplayer
		if (mode == 0) {
			while (win == false) {
				if(turn == 9){
					JOptionPane.showMessageDialog(null, "Game Is A Draw\n" + BoardToText(board), "Result", JOptionPane.INFORMATION_MESSAGE);
					break;
				}

				//play for o
				if ((turn % 2) == 0) {

					//asking for user input
					change = JOptionPane.showOptionDialog(null, "pick a spot:\n" + BoardToText(board), "Turn for O", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, board, board[0]);

						character = board[change];
					
						//figuring out what number to change in the board array
						switch (character) {
							case "X":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE AN O HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "O":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE AN O HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "1":
								board[0] = "O";
								turn++;
								break;
							case "2":
								board[1] = "O";
								turn++;
								break;
							case "3":
								board[2] = "O";
								turn++;
								break;
							case "4":
								board[3] = "O";
								turn++;
								break;
							case "5":
								board[4] = "O";
								turn++;
								break;
							case "6":
								board[5] = "O";
								turn++;
								break;
							case "7":
								board[6] = "O";
								turn++;
								break;
							case "8":
								board[7] = "O";
								turn++;
								break;
							case "9":
								board[8] = "O";
								turn++;
								break;
						}
						
					
						if(WinCheck(board) == "O"){
							JOptionPane.showMessageDialog(null, "The winner is O\n" + BoardToText(board), "winner", JOptionPane.INFORMATION_MESSAGE);
							win = true;
							System.exit(0);
						}
				} //play for x(computer)
				else {
					change = comp(board);
					character = board[change];
//						//figuring out what number to change in the board array
					
						switch (character) {
							case "X":
								break;
							case "O":
								break;
							case "1":
								board[0] = "X";
								turn++;
								break;
							case "2":
								board[1] = "X";
								turn++;
								break;
							case "3":
								board[2] = "X";
								turn++;
								break;
							case "4":
								board[3] = "X";
								turn++;
								break;
							case "5":
								board[4] = "X";
								turn++;
								break;
							case "6":
								board[5] = "X";
								turn++;
								break;
							case "7":
								board[6] = "X";
								turn++;
								break;
							case "8":
								board[7] = "X";
								turn++;
								break;
							case "9":
								board[8] = "X";
								turn++;
								break;
						}
						
						
						
						if(WinCheck(board) == "X"){
							JOptionPane.showMessageDialog(null, "The winner is X\n" + BoardToText(board), "winner", JOptionPane.INFORMATION_MESSAGE);
							win = true;
							System.exit(0);
						}
					}
			
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//multiplayer
		if (mode == 1) {
			while (win == false) {
				if(turn == 9){
					JOptionPane.showMessageDialog(null, "Game Is A Draw\n" + BoardToText(board), "Result", JOptionPane.INFORMATION_MESSAGE);
					break;
				}

				//play for o
				if ((turn % 2) == 0) {
					

					//asking for user input
					change = JOptionPane.showOptionDialog(null, "pick a spot:\n" + BoardToText(board), "Turn for O", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, board, board[0]);

						character = board[change];
					
						//figuring out what number to change in the board array
						switch (character) {
							case "X":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE AN O HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "O":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE AN O HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "1":
								board[0] = "O";
								turn++;
								break;
							case "2":
								board[1] = "O";
								turn++;
								break;
							case "3":
								board[2] = "O";
								turn++;
								break;
							case "4":
								board[3] = "O";
								turn++;
								break;
							case "5":
								board[4] = "O";
								turn++;
								break;
							case "6":
								board[5] = "O";
								turn++;
								break;
							case "7":
								board[6] = "O";
								turn++;
								break;
							case "8":
								board[7] = "O";
								turn++;
								break;
							case "9":
								board[8] = "O";
								turn++;
								break;
						}
						
					
						if(WinCheck(board) == "O"){
							JOptionPane.showMessageDialog(null, "The winner is O\n" + BoardToText(board), "winner", JOptionPane.INFORMATION_MESSAGE);
							win = true;
							System.exit(0);
						}
				} //play for x
				else {
					
					//asking for user input
					change = JOptionPane.showOptionDialog(null, "pick a spot:\n" + BoardToText(board), "Turn for X", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, board, board[0]);
					character = board[change];
//						//figuring out what number to change in the board array
					
						switch (character) {
							case "X":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE A X HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "O":
								JOptionPane.showMessageDialog(null, "YOU CANNOT PLACE A X HERE", "error", JOptionPane.ERROR_MESSAGE);
								break;
							case "1":
								board[0] = "X";
								turn++;
								break;
							case "2":
								board[1] = "X";
								turn++;
								break;
							case "3":
								board[2] = "X";
								turn++;
								break;
							case "4":
								board[3] = "X";
								turn++;
								break;
							case "5":
								board[4] = "X";
								turn++;
								break;
							case "6":
								board[5] = "X";
								turn++;
								break;
							case "7":
								board[6] = "X";
								turn++;
								break;
							case "8":
								board[7] = "X";
								turn++;
								break;
							case "9":
								board[8] = "X";
								turn++;
								break;
						}
						
						
						System.out.println(WinCheck(board));
						if(WinCheck(board) == "X"){
							JOptionPane.showMessageDialog(null, "The winner is X\n" + BoardToText(board), "winner", JOptionPane.INFORMATION_MESSAGE);
							win = true;
							System.exit(0);
						}
					}
			}
		}

	}

	public static String BoardToText(String[] Board) {
		String out = null;
		Object[] inboard = new Object[9];
		inboard = Board;
		out = (inboard[0] + "  |  " + inboard[1] + "  |  " + inboard[2]
				+ "\n------------\n"
				+ inboard[3] + "  |  " + inboard[4] + "  |  " + inboard[5]
				+ "\n------------\n"
				+ inboard[6] + "  |  " + inboard[7] + "  |  " + inboard[8]);

		return out;
	}
	


	
	
	public static int comp(String[] Board){

		int pos = 0;

//		ArrayList<String> available = new ArrayList<String>();
//		available.add("1");
//		available.add("2");
//		available.add("3");
//		available.add("4");
//		available.add("5");
//		available.add("6");
//		available.add("7");
//		available.add("8");
//		available.add("9");

		Random rand = new Random();
		pos = rand.nextInt(8);

		if(Board[0] == "X" && Board[1] == "X"){
			pos = 2;
		}if(Board[0] == "X" && Board[2] == "X"){
			pos = 1;
		}if(Board[1] == "X" && Board[2] == "X"){
			pos = 0;
		}
		
		if(Board[3] == "X" && Board[4] == "X"){
			pos = 5;
		}if(Board[3] == "X" && Board[5] == "X"){
			pos = 4;
		}if(Board[4] == "X" && Board[5] == "X"){
			pos = 3;
		}
		
		if(Board[6] == "X" && Board[7] == "X"){
			pos = 8;
		}if(Board[6] == "X" && Board[8] == "X"){
			pos = 7;
		}if(Board[7] == "X" && Board[8] == "X"){
			pos = 6;
		}		
		
		if(Board[0] == "X" && Board[3] == "X"){
			pos = 6;
		}if(Board[0] == "X" && Board[6] == "X"){
			pos = 3;
		}if(Board[3] == "X" && Board[6] == "X"){
			pos = 0;
		}
		
				
		if(Board[1] == "X" && Board[4] == "X"){
			pos = 7;
		}if(Board[1] == "X" && Board[7] == "X"){
			pos = 4;
		}if(Board[4] == "X" && Board[7] == "X"){
			pos = 1;
		}
		
				
		if(Board[2] == "X" && Board[5] == "X"){
			pos = 8;
		}if(Board[2] == "X" && Board[8] == "X"){
			pos = 5;
		}if(Board[5] == "X" && Board[8] == "X"){
			pos = 2;
		}
		
				
		if(Board[0] == "X" && Board[4] == "X"){
			pos = 8;
		}if(Board[0] == "X" && Board[8] == "X"){
			pos = 4;
		}if(Board[4] == "X" && Board[8] == "X"){
			pos = 0;
		}
		
				
		if(Board[2] == "X" && Board[4] == "X"){
			pos = 6;
		}if(Board[2] == "X" && Board[6] == "X"){
			pos = 4;
		}if(Board[4] == "X" && Board[6] == "X"){
			pos = 2;
		}
		
//		if(Board[0] == "X"){
//			row[0]++;
//			col[0]++;
//			dial[0]++;
//			available.remove("1");
//		} if(Board[1] == "X"){
//			row[0]++;
//			col[0]++;
//			available.remove("2");
//		} if(Board[2] == "X"){
//			row[0]++;
//			col[2]++;;
//			diar[0]++;
//			available.remove("3");
//		} if(Board[3] == "X"){
//			row[1]++;
//			col[0]++;
//			available.remove("4");
//		} if(Board[4] == "X"){
//			row[1]++;
//			col[1]++;
//			dial[1]++;
//			diar[1]++;
//			available.remove("5");
//		} if(Board[5] == "X"){
//			row[1]++;
//			col[2]++;
//			available.remove("6");
//		} if(Board[6] == "X"){
//			row[2]++;
//			col[0]++;
//			diar[2]++;
//			available.remove("7");
//		} if(Board[7] == "X"){
//			row[2]++;
//			col[1]++;
//			available.remove("8");
//		} if(Board[8] == "X"){
//			row[2]++;
//			col[2]++;
//			dial[2]++;
//			available.remove("9");
//		}
//		
//		
//		if(Board[0] == "O"){
//			available.remove("1");
//		}if(Board[1] == "O"){
//			available.remove("2");
//		}if(Board[2] == "O"){
//			available.remove("3");
//		}if(Board[3] == "O"){
//			available.remove("4");
//		}if(Board[4] == "O"){
//			available.remove("5");
//		}if(Board[5] == "O"){
//			available.remove("6");
//		}if(Board[6] == "O"){
//			available.remove("7");
//		}if(Board[7] == "O"){
//			available.remove("8");
//		}if(Board[8] == "O"){
//			available.remove("9");
//		}
		
//		System.out.println(Arrays.toString(row));
//		System.out.println(Arrays.toString(col));
//		System.out.println(Arrays.toString(dial));
//		System.out.println(Arrays.toString(diar));
//		
//		int randomNumber = rand.nextInt(available.toArray().length-1);
//      	pos = Integer.parseInt((String) available.toArray()[randomNumber]);
		
	
		
		return pos;
	}
	
			

	
	static String[] toprow = {"0", "1", "2"};
	static String[] midrow = {"3", "4", "5"};
	static String[] botrow = {"6", "7", "8"};
	static String[] lefcol = {"0", "3", "6"};
	static String[] midcol = {"1", "4", "7"};
	static String[] rigcol = {"2", "5", "8"};
	static String[] diaL = {"0", "4", "8"};
	static String[] diaR = {"2", "4", "6"};
	public static String WinCheck(String[] Board){
		String out = null;
		
		
		if(Board[0] == "X"){
			toprow[0] = "X";
			lefcol[0] = "X";
			diaL[0] = "X";
		} if(Board[1] == "X"){
			toprow[1] = "X";
			midcol[0] = "X";
		} if(Board[2] == "X"){
			toprow[2] = "X";
			rigcol[0] = "X";
			diaR[0] = "X";
		} if(Board[3] == "X"){
			midrow[0] = "X";
			lefcol[1] = "X"; 
		} if(Board[4] == "X"){
			midrow[1] = "X";
			midcol[1] = "X";
			diaL[1] = "X";
			diaR[1] = "X";
		} if(Board[5] == "X"){
			midrow[2] = "X";
			rigcol[1] = "X";
		} if(Board[6] == "X"){
			botrow[0] = "X";
			lefcol[2] = "X";
			diaR[2] = "X";
		} if(Board[7] == "X"){
			botrow[1] = "X";
			midcol[2] = "X";
		} if(Board[8] == "X"){
			botrow[2] = "X";
			rigcol[2] = "X";
			diaL[2] = "X";
		}
		
		
		if(Board[0] == "O"){
			toprow[0] = "O";
			lefcol[0] = "O";
			diaL[0] = "O";
		} if(Board[1] == "O"){
			toprow[1] = "O";
			midcol[0] = "O";
		} if(Board[2] == "O"){
			toprow[2] = "O";
			rigcol[0] = "O";
			diaR[0] = "O";
		} if(Board[3] == "O"){
			midrow[0] = "O";
			lefcol[1] = "O"; 
		} if(Board[4] == "O"){
			midrow[1] = "O";
			midcol[1] = "O";
			diaL[1] = "O";
			diaR[1] = "O";
		} if(Board[5] == "O"){
			midrow[2] = "O";
			rigcol[1] = "O";
		} if(Board[6] == "O"){
			botrow[0] = "O";
			lefcol[2] = "O";
			diaR[2] = "O";
		} if(Board[7] == "O"){
			botrow[1] = "O";
			midcol[2] = "O";
		} if(Board[8] == "O"){
			botrow[2] = "O";
			rigcol[2] = "O";
			diaL[2] = "O";
		}
		

		if(botrow[0] == "X" && botrow[1] == "X" && botrow[2] == "X"){
			out = "X";
		}
		if(toprow[0] == "X" && toprow[1] == "X" && toprow[2] == "X"){
			out = "X";
		}
		if(midrow[0] == "X" && midrow[1] == "X" && midrow[2] == "X"){
			out = "X";
		}
		if(lefcol[0] == "X" && lefcol[1] == "X" && lefcol[2] == "X"){
			out = "X";
		}
		if(midcol[0] == "X" && midcol[1] == "X" && midcol[2] == "X"){
			out = "X";
		}		
		if(rigcol[0] == "X" && rigcol[1] == "X" && rigcol[2] == "X"){
			out = "X";
		}				
		if(diaL[0] == "X" && diaL[1] == "X" && diaL[2] == "X"){
			out = "X";
		}
		if(diaR[0] == "X" && diaR[1] == "X" && diaR[2] == "X"){
			out = "X";
		}
		
		
		
		if(botrow[0] == "O" && botrow[1] == "O" && botrow[2] == "O"){
			out = "O";
		}
		if(toprow[0] == "O" && toprow[1] == "O" && toprow[2] == "O"){
			out = "O";
		}
		if(midrow[0] == "O" && midrow[1] == "O" && midrow[2] == "O"){
			out = "O";
		}
		if(lefcol[0] == "O" && lefcol[1] == "O" && lefcol[2] == "O"){
			out = "O";
		}
		if(midcol[0] == "O" && midcol[1] == "O" && midcol[2] == "O"){
			out = "O";
		}		
		if(rigcol[0] == "O" && rigcol[1] == "O" && rigcol[2] == "O"){
			out = "O";
		}				
		if(diaL[0] == "O" && diaL[1] == "O" && diaL[2] == "O"){
			out = "O";
		}
		if(diaR[0] == "O" && diaR[1] == "O" && diaR[2] == "O"){
			out = "O";
		}
				
		return out;
		
		

//		System.out.println(Arrays.toString(inboard));
//
//		if(inboard[0] == "X"){
//			rowX[0] = rowX[0] + 1;
//			colX[0] = colX[0] + 1;
//			diaX[0] = diaX[0] + 1;
//		}
//		if(inboard[1] == "X"){
//			rowX[0] = rowX[0] + 1;
//			colX[1] = colX[1] + 1;
//		}
//		if(inboard[2] == "X"){
//			rowX[0] = rowX[0] + 1;
//			colX[2] = colX[2] + 1;
//			diaX[2] = diaX[2] + 1;
//		}
//		if(inboard[3] == "X"){
//			rowX[1] = rowX[1] + 1;
//			colX[0] = colX[0] + 1;
//		}
//		if(inboard[4] == "X"){
//			rowX[1] = rowX[1] + 1;
//			colX[1] = colX[1] + 1;		
//			diaX[1] = diaX[1] + 1;
//		}
//		if(inboard[5] == "X"){
//			rowX[1] = rowX[1] + 1;
//			colX[2] = colX[2] + 1;		
//		}
//		if(inboard[6] == "X"){
//			rowX[2] = rowX[2] + 1;	
//			colX[0] = colX[0] + 1;
//			diaX[0] = diaX[0] + 1;
//		}
//		if(inboard[7] == "X"){
//			rowX[2] = rowX[2] + 1;
//			colX[1] = colX[1] + 1;
//		}
//		if(inboard[8] == "X"){
//			rowX[2] = rowX[2] + 1;
//			colX[2] = colX[2] + 1;
//		}
//			
//
//		if(inboard[0] == "O"){
//			rowO[0] = rowO[0] + 1;
//			colO[0] = colO[0] + 1;
//			diaO[0] = diaO[0] + 1;
//		}
//		if(inboard[1] == "O"){
//			rowO[0] = rowO[0] + 1;
//			colO[1] = colO[1] + 1;
//		}
//		if(inboard[2] == "O"){
//			rowO[0] = rowO[0] + 1;
//			colO[2] = colO[2] + 1;
//			diaO[2] = diaO[2] + 1;
//		}
//		if(inboard[3] == "O"){
//			rowO[1] = rowO[1] + 1;
//			colO[0] = colO[0] + 1;
//		}
//		if(inboard[4] == "O"){
//			rowO[1] = rowO[1] + 1;
//			colO[1] = colO[1] + 1;		
//			diaO[1] = diaO[1] + 1;	
//		}
//		if(inboard[5] == "O"){
//			rowO[1] = rowO[1] + 1;
//			colO[2] = colO[2] + 1;	
//		}
//		if(inboard[6] == "O"){
//			rowO[2] = rowO[2] + 1;	
//			colO[0] = colO[0] + 1;
//			diaO[0] = diaO[0] + 1;
//		}
//		if(inboard[7] == "O"){
//			rowO[2] = rowO[2] + 1;
//			colO[1] = colO[1] + 1;
//		}
//		if(inboard[8] == "O"){
//			rowO[2] = rowO[2] + 1;
//			colO[2] = colO[2] + 1;
//		}
//			
//		if((rowX[0] == 1 && rowX[1] == 1 && rowX[2] == 1) && (colX[0] == 1 && colX[1] == 1 && colX[2] == 1)){
//			System.out.println("X wins");
//			out = "X";
//		}
			
//		System.out.println(Arrays.toString(rowX));
//		System.out.println(Arrays.toString(colX));
//		System.out.println(Arrays.toString(diaX));
//		System.out.println(Arrays.toString(rowO));
//		System.out.println(Arrays.toString(colO));
//		System.out.println(Arrays.toString(diaO));
		
	}
	
	
}
